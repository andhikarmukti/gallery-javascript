const jumbo  = document.querySelector('.jumbo');
const thumb  = document.querySelectorAll('.thumb');
const thumbs = document.querySelectorAll('.thumb');

// thumb.forEach(function(t){
//     t.addEventListener('click', function(timg){
//         jumbo.setAttribute('src', timg.target.getAttribute('src'));
//     });
// });

document.querySelector('.container').addEventListener('click', function(e){
    if(e.target.className == 'thumb'){
        // jumbo.setAttribute('src', e.target.getAttribute('src'));
        jumbo.src = e.target.src;
        jumbo.classList.add('fade');
        setTimeout(function(){
            jumbo.classList.remove('fade');
        }, 500);
    }

    thumbs.forEach(function(tSelected){
        tSelected.className = 'thumb';
    });

    e.target.classList.add('selected');
});

